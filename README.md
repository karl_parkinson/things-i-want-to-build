# Things-I-Want-to-Build

* web app to text me the weather every morning
* wi-fi controlled deadbolt lock
* voice controlled drone
* automated sports betting system
* online book publishing platform
* functional compiler in Haskell
* unbeatable tic-tac-toe AI
* RESTful API of some sort
* resume generator from github profile
* OSS contributions. preferably something small and in ruby or python. maybe classtime
* simple chat application using Haskell and websockets.
